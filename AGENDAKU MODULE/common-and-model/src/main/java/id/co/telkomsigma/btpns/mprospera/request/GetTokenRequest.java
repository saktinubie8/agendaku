package id.co.telkomsigma.btpns.mprospera.request;

public class GetTokenRequest extends BaseRequest {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}