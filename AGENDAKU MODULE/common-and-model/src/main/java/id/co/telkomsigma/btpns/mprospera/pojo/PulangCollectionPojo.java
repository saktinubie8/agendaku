package id.co.telkomsigma.btpns.mprospera.pojo;

import javax.persistence.*;
import java.math.BigInteger;

@SqlResultSetMapping(name = "PulangCollectionPojo", entities = {
        @EntityResult(entityClass = PulangCollectionPojo.class, fields = {
                @FieldResult(name = "sentraId", column = "sentra_id"),
                @FieldResult(name = "amount", column = "amount"),
                @FieldResult(name = "wismaCode", column = "code"),
                @FieldResult(name = "sentraName", column = "sentra_name")
        })})

@Entity
public class PulangCollectionPojo {

    @Id
    public BigInteger sentraId;
    public BigInteger amount;
    public String wismaCode;
    public String sentraName;

    public BigInteger getSentraId() {
        return sentraId;
    }

    public void setSentraId(BigInteger sentraId) {
        this.sentraId = sentraId;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public String getWismaCode() {
        return wismaCode;
    }

    public void setWismaCode(String wismaCode) {
        this.wismaCode = wismaCode;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

}