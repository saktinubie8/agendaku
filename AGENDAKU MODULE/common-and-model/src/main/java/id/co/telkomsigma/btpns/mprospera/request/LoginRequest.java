package id.co.telkomsigma.btpns.mprospera.request;

public class LoginRequest extends BaseRequest {

    private String username;
    private String password;
    private String imei;
    private String androidVersion;
    private String modelNumber;
    private String androidBuildNumber;
    private String androidBasebandVersion;
    private String processor;
    private String ram;
    private String gpsVersion;
    private String totalDeviceMemory;
    private String freeDeviceMemory;
    private String errorLog;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getAndroidBuildNumber() {
        return androidBuildNumber;
    }

    public void setAndroidBuildNumber(String androidBuildNumber) {
        this.androidBuildNumber = androidBuildNumber;
    }

    public String getAndroidBasebandVersion() {
        return androidBasebandVersion;
    }

    public void setAndroidBasebandVersion(String androidBasebandVersion) {
        this.androidBasebandVersion = androidBasebandVersion;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGpsVersion() {
        return gpsVersion;
    }

    public void setGpsVersion(String gpsVersion) {
        this.gpsVersion = gpsVersion;
    }

    public String getTotalDeviceMemory() {
        return totalDeviceMemory;
    }

    public void setTotalDeviceMemory(String totalDeviceMemory) {
        this.totalDeviceMemory = totalDeviceMemory;
    }

    public String getFreeDeviceMemory() {
        return freeDeviceMemory;
    }

    public void setFreeDeviceMemory(String freeDeviceMemory) {
        this.freeDeviceMemory = freeDeviceMemory;
    }

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }

    @Override
    public String toString() {
        return "LoginRequest [username=" + username + ", imei=" + imei + ", androidVersion=" + androidVersion
                + ", modelNumber=" + modelNumber + ", androidBuildNumber=" + androidBuildNumber
                + ", androidBasebandVersion=" + androidBasebandVersion + ", processor=" + processor + ", ram=" + ram
                + ", gpsVersion=" + gpsVersion + ", totalDeviceMemory=" + totalDeviceMemory + ", freeDeviceMemory="
                + freeDeviceMemory + ", errorLog=" + errorLog + "]";
    }

}