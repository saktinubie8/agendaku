package id.co.telkomsigma.btpns.mprospera.request;

public class BringMoneyPRSRequest extends BaseRequest {

    private String wismaCode;
    private String coCode;
    private String username;
    private String sessionKey;
    private String prsDate;

    public String getWismaCode() {
        return wismaCode;
    }

    public void setWismaCode(String wismaCode) {
        this.wismaCode = wismaCode;
    }

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getPrsDate() {
        return prsDate;
    }

    public void setPrsDate(String prsDate) {
        this.prsDate = prsDate;
    }

}