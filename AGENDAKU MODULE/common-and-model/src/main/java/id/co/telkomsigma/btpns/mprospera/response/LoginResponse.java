package id.co.telkomsigma.btpns.mprospera.response;

public class LoginResponse extends ProsperaLoginResponse {

    private String sessionKey;
    private String limitBwmp;
    private String downloadUrl;
    private String userId;
    private String officeName;
    private String officeId;
    private String officeCode;
    private String officeStatus;
    private String kfoName;
    private String kfoCode;
    private String tokenKey;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getLimitBwmp() {
        return limitBwmp;
    }

    public void setLimitBwmp(String limitBwmp) {
        this.limitBwmp = limitBwmp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    @Override
    public String getOfficeId() {
        return officeId;
    }

    @Override
    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getKfoName() {
        return kfoName;
    }

    public void setKfoName(String kfoName) {
        this.kfoName = kfoName;
    }

    public String getKfoCode() {
        return kfoCode;
    }

    public void setKfoCode(String kfoCode) {
        this.kfoCode = kfoCode;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getOfficeStatus() {
        return officeStatus;
    }

    public void setOfficeStatus(String officeStatus) {
        this.officeStatus = officeStatus;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "sessionKey='" + sessionKey + '\'' +
                ", limitBwmp='" + limitBwmp + '\'' +
                ", downloadUrl='" + downloadUrl + '\'' +
                ", userId='" + userId + '\'' +
                ", officeName='" + officeName + '\'' +
                ", officeId='" + officeId + '\'' +
                ", officeCode='" + officeCode + '\'' +
                ", officeStatus='" + officeStatus + '\'' +
                ", kfoName='" + kfoName + '\'' +
                ", kfoCode='" + kfoCode + '\'' +
                ", tokenKey='" + tokenKey + '\'' +
                '}';
    }

}