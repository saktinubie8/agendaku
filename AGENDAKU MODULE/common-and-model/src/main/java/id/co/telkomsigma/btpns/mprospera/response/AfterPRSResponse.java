package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class AfterPRSResponse extends BaseResponse {

    public class TypeDetailsPojo {
        private String appOrCenterId;
        private String name;
        private String amount;

        public String getAppOrCenterId() {
            return appOrCenterId;
        }

        public void setAppOrCenterId(String appOrCenterId) {
            this.appOrCenterId = appOrCenterId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    public String mmsCode;
    public String coCode;
    public String type;
    public List<TypeDetailsPojo> typeDetails;

    public String getMmsCode() {
        return mmsCode;
    }

    public void setMmsCode(String mmsCode) {
        this.mmsCode = mmsCode;
    }

    public String getCoCode() {
        return coCode;
    }

    public void setCoCode(String coCode) {
        this.coCode = coCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<TypeDetailsPojo> getTypeDetails() {
        if (typeDetails == null) {
            typeDetails = new ArrayList<AfterPRSResponse.TypeDetailsPojo>();
        }
        return typeDetails;
    }

    public void setTypeDetails(List<TypeDetailsPojo> typeDetails) {
        this.typeDetails = typeDetails;
    }

}