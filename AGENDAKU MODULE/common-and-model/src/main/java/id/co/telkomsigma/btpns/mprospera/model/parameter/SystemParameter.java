package id.co.telkomsigma.btpns.mprospera.model.parameter;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "M_PARAMETER")
public class SystemParameter extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long parameterId;
    private String paramGroup;
    private String paramName;
    private String paramValue;
    private String paramDescription;
    private User createdBy;
    private Date createdDate;
    private User updatedBy;
    private Date updatedDate;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getParameterId() {
        return parameterId;
    }

    @Column(name = "PARAM_GROUP", nullable = false, length = MAX_LENGTH_PARAM_GROUP)
    public String getParamGroup() {
        return paramGroup;
    }

    @Column(name = "PARAM_NAME", nullable = false, length = MAX_LENGTH_PARAM_NAME)
    public String getParamName() {
        return paramName;
    }

    @Column(name = "PARAM_VALUE", nullable = false, length = MAX_LENGTH_PARAM_VALUE)
    public String getParamValue() {
        return paramValue;
    }

    @Column(name = "PARAM_DESCRIPTION", nullable = false, length = MAX_LENGTH_DESCRIPTION)
    public String getParamDescription() {
        return paramDescription;
    }

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "CREATED_BY", nullable = false)
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "UPDATE_BY", nullable = false)
    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updateBy) {
        this.updatedBy = updateBy;
    }

    @Column(name = "UPDATE_DT", nullable = false)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updateDate) {
        this.updatedDate = updateDate;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public void setParamGroup(String paramGroup) {
        this.paramGroup = paramGroup;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

}