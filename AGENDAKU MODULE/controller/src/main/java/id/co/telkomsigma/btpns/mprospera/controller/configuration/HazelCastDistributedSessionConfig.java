package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@SuppressWarnings("ALL")
@Configuration
@EnableHazelcastHttpSession
public class HazelCastDistributedSessionConfig {

    protected final Log log = LogFactory.getLog(getClass());

    @Bean(name = "hzInstance")
    public HazelcastInstance hazelcastInstance() {
        log.info("Initialize Hazelcast Distributed Client Config");
        HazelcastInstance instance = null;
        try {
            ClientConfig hazelcastConfig = new XmlClientConfigBuilder("hazelcast-client.xml").build();
            hazelcastConfig.setConnectionAttemptLimit(100);
            hazelcastConfig.setConnectionAttemptPeriod(5000);
            instance = HazelcastClient.newHazelcastClient(hazelcastConfig);
        } catch (Exception e) {
            log.error("Failed connect to Hazelcast Server", e);
        }
        return instance;
    }

    @Bean
    public CacheManager cacheManager() {
        log.info("Registering Hazelcast Cache Manager");
        HazelcastInstance instance = hazelcastInstance();
        CacheManager manager = new HazelcastCacheManager(instance);
        return manager;
    }

}