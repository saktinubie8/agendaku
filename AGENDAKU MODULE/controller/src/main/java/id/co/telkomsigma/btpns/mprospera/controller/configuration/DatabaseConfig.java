package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
@EnableConfigurationProperties({DataSourceProperties.class})
public class DatabaseConfig {

    protected Log log = LogFactory.getLog(this.getClass());

    @Bean(destroyMethod = "", name = "primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    @ConditionalOnProperty(prefix = "spring.datasource", name = {"jndi-name"})
    public DataSource primaryDataSource(DataSourceProperties properties) {
        JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = dataSourceLookup.getDataSource(properties.getJndiName());
        System.out.println("SQL JNDI" + properties.getJndiName());
        log.info("SQL JNDI" + properties.getJndiName());
        return dataSource;
    }
  /* @Bean(name = "primaryDataSource")
   @Qualifier("primaryDataSource")
   @Primary
   @ConfigurationProperties(prefix = "spring.datasource")
   @ConditionalOnProperty(prefix = "spring.datasource", name = { "url" })
   public DataSource primary() {
       return DataSourceBuilder.create().build();
   }*/

}