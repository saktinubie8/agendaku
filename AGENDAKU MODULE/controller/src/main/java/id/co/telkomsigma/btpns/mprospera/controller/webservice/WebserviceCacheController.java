package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Dzulfiqar on 19/07/2017.
 */
@RestController
public class WebserviceCacheController extends GenericController {

    @Autowired
    CacheService cacheService;

    @RequestMapping(value = WebGuiConstant.CLEAR_ALL_CACHE, method = RequestMethod.POST)
    public void clearCache() {
        log.info("Clear All Cache");
        try {
            cacheService.clearCache();
        } catch (Exception e) {
            log.error("ERROR CLEAR CACHE, Class WebServiceCacheController " + e.getMessage());
        }
    }

}