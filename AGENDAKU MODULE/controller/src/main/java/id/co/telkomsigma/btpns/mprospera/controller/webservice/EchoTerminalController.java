package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.request.EchoRequest;

import java.io.IOException;

import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//ECHO TEST CONNECTION FROM ANDROID TO M-PROSPERA API SERVER
@RestController
public class EchoTerminalController extends GenericController {

    @RequestMapping(value = WebGuiConstant.TERMINAL_ECHO_PATH, method = RequestMethod.POST)
    public @ResponseBody
    BaseResponse echo(@RequestBody EchoRequest request) throws IOException {
        BaseResponse response = new BaseResponse();
        log.info("ECHO Requested");
        if (request.getRequestCode().equals("ECHO")) {
            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
            response.setResponseMessage("SUCCESS");
        } else {
            response.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
            response.setResponseMessage("FAILED");
        }
        log.info("Response message : " + response.toString());
        return response;
    }

}