package id.co.telkomsigma.btpns.mprospera.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class JWTUtils {

    @Autowired
    ParameterService parameterService;

    private static String secret;

    private static Base64.Decoder decoder = Base64.getDecoder();

    @PostConstruct
    protected void init() {
        secret = parameterService.loadParamByParamName(WebGuiConstant.PARAMETER_SECRET_KEY, "mpros-secret");
    }

    public static String createJwt(String username) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date d = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println(dateFormat.format(d));
        Map<String, Object> headerClaims = new HashMap();
        headerClaims.put("owner", "mprospera");
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String jwtToken = JWT.create()
                    .withHeader(headerClaims)
                    .withIssuer(username)
                    .withJWTId(UUID.randomUUID().toString())
                    .withExpiresAt(d)
                    .sign(algorithm);
            return jwtToken;
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            return "FAILED";
        }

    }

    public static Boolean verifyJwt(String token, String username) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(username)
                    .acceptExpiresAt(10)
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            System.out.println("Check Token SUCCESS");
            byte[] headerByte = Base64.getDecoder().decode(jwt.getHeader());
            String header = new String(headerByte);
            Map<String, String> map = null;
            try {
                map = mapper.readValue(header, Map.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (map.get("owner").equals("mprospera")) {
                return true;
            } else {
                return false;
            }
        } catch (JWTVerificationException exception) {
            //Invalid signature/claims
            exception.printStackTrace();
            System.out.println("JWT Token False");
            return false;
        }
    }

}