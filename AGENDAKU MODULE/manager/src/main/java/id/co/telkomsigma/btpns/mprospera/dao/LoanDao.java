package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;

public interface LoanDao extends JpaRepository<Loan, Long> {

    @Query(value = "select sum(la.plafond) from t_loan_account la inner join t_customer c \r\n" +
            "on la.customer_id = c.id inner join t_sentra_group sg \r\n" +
            "on c.group_id = sg.id inner join t_sentra ts\r\n" +
            "on sg.sentra_id = ts.id inner join m_location ml\r\n" +
            "on ts.location_id = ml.id\r\n" +
            "where CONVERT (date, disbursement_date) = dateadd (day,14,:disbrusment) and la.status =:status and ml.code =:wismaCode", nativeQuery = true)
    BigDecimal getplafon(@Param("disbrusment") Date date, @Param("wismaCode") String wismaCode, @Param("status") String status);

}