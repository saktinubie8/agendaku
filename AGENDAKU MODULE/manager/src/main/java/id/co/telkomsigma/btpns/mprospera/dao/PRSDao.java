package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanPRS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;

public interface PRSDao extends JpaRepository<LoanPRS, Long> {

    @Query(value = "select SUM(lp.disbursement_amt) as disbursement_amt\n" +
            "from t_loan_prs lp \n" +
            "inner join t_prs p on lp.prs_id=p.id\n" +
            "inner join t_sentra s on p.sentra_id=s.id\n" +
            "inner join m_location l on s.location_id=l.id\n" +
            "where l.code=:wismaCode and p.prs_date=:date", nativeQuery = true)
    BigDecimal getDisbursementAmt(@Param("wismaCode") String wismaCode, @Param("date") Date date);

    @Query(value = "select SUM(lp.withdrawal_amt_plan) as withdrawal_amt_plan\n" +
            "from t_saving_prs lp \n" +
            "inner join t_prs p on lp.prs_id=p.id\n" +
            "inner join t_sentra s on p.sentra_id=s.id\n" +
            "inner join m_location l on s.location_id=l.id\n" +
            "where l.code=:wismaCode and p.prs_date=:date", nativeQuery = true)
    BigDecimal getWithdrawalAmtPlan(@Param("wismaCode") String wismaCode, @Param("date") Date date);

    @Query(value = "select withdrawal_amt_plan_next from t_saving_prs lp\r\n" +
            "inner join t_prs p on lp.prs_id=p.id\r\n" +
            "inner join t_sentra s on p.sentra_id=s.id\r\n" +
            "inner join m_location l on s.location_id=l.id\r\n" +
            "where l.code=:wismaCode and prs_id in \r\n" +
            "(select id from t_prs where prs_date =:date and \r\n" +
            "prs_status in ('SUBMIT','APPROVED','MANUAL_KFO'))", nativeQuery = true)
    BigDecimal getwithdrawalamtplannext(@Param("wismaCode") String wismaCode, @Param("date") Date date);

    @Query(value = "select SUM(p.bring_money) as bring_money\r\n" +
            "from  t_prs p\r\n" +
            "inner join t_sentra s on p.sentra_id=s.id\r\n" +
            "inner join m_location l on s.location_id=l.id\r\n" +
            "where l.code=:wismaCode and p.prs_date=:date and s.assign_to=:coCode\r\n" +
            "and p.prs_status in ('APPROVED','MANUAL_KFO','SUBMIT')", nativeQuery = true)
    BigDecimal getBringMoney(@Param("wismaCode") String wismaCode, @Param("date") Date date, @Param("coCode") String coCode);

}