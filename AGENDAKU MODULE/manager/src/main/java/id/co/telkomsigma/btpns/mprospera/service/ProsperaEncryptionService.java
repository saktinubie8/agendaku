package id.co.telkomsigma.btpns.mprospera.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("prosperaEncryptionService")
public class ProsperaEncryptionService extends GenericService {

    private static final String CRYPTO_ALGORITHM = "AES";
    private static final String DIGEST_ALGORITHM = "SHA-256";
    private static final String CHARSET_UTF8 = "utf-8";
    private IvParameterSpec iv;
    private SecretKeySpec secretKey;
    private MessageDigest md;

    @Autowired
    ParameterService parameterService;

    @PostConstruct
    private void init() {
        try {
            Security.addProvider(new BouncyCastleProvider());
            md = MessageDigest.getInstance(DIGEST_ALGORITHM);

            String strIv = parameterService.loadParamByParamName("Crypto.Iv", "THISISKEYFORIV");
            byte[] temp = Arrays.copyOf(strIv.getBytes(), 16);
            iv = new IvParameterSpec(temp);

            secretKey = generateKey(parameterService.loadParamByParamName("Crypto.Key", "THISISKEYFORCRYPTO"));
        } catch (NoSuchAlgorithmException e) {

        }
    }

    public String encryptField(String field) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance(CRYPTO_ALGORITHM);
            cipher.init(1, secretKey, iv);
            byte[] cipherText = new byte[cipher.getOutputSize(field.getBytes().length)];
            int ctLength = cipher.update(field.getBytes(), 0, field.getBytes().length, cipherText, 0);
            ctLength += cipher.doFinal(cipherText, ctLength);
            return new String(Base64.encodeBase64(cipherText), CHARSET_UTF8);
        } catch (Exception e) {
            // TODO: handle exception
            throw e;
        }
    }

    public String decryptField(String field) throws Exception {
        // TODO Auto-generated method stub
        log.debug("Message to encrypt >> " + field);
        try {
            Cipher cipher = Cipher.getInstance(CRYPTO_ALGORITHM);
            cipher.init(2, secretKey, iv);
            byte[] decodeStr = Base64.decodeBase64(field.getBytes(CHARSET_UTF8));
            byte[] cipherText = new byte[cipher.getOutputSize(decodeStr.length)];
            int ctLength = cipher.update(decodeStr, 0, decodeStr.length, cipherText, 0);
            ctLength += cipher.doFinal(cipherText, ctLength);
            byte[] trimmed = trim(cipherText);
            return new String(trimmed, CHARSET_UTF8);
        } catch (Exception e) {
            throw e;
        }
    }

    private SecretKeySpec generateKey(String key) {
        SecretKeySpec secretKey = null;
        try {
            byte[] byteKey = md.digest(key.getBytes(CHARSET_UTF8));
            secretKey = new SecretKeySpec(byteKey, CRYPTO_ALGORITHM);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return secretKey;
    }

    static byte[] trim(byte[] bytes) {
        int i = bytes.length - 1;
        while ((i >= 0) && (bytes[i] == 0)) {
            --i;
        }
        return Arrays.copyOf(bytes, i + 1);
    }

    public static void main(String args[]) {
        ProsperaEncryptionService a = new ProsperaEncryptionService();
        a.init();
        try {
            System.out.println(a.encryptField("P@ssw0rd"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
