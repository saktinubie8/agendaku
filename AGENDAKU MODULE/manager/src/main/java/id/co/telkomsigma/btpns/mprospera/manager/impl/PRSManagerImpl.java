package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.LoanDao;
import id.co.telkomsigma.btpns.mprospera.dao.PRSDao;
import id.co.telkomsigma.btpns.mprospera.manager.PRSManager;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangCollectionPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangPRSpojo;

@Service("prsManager")
public class PRSManagerImpl implements PRSManager{

	private final Log log = LogFactory.getLog(getClass());
	
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PRSDao prsDao;
	
	@Autowired 
	LoanDao loanDao;
	
	@Override
	public BigDecimal getDisbursementAmt(String wismaCode, Date prsDate) {
		return prsDao.getDisbursementAmt(wismaCode, prsDate);
	}

	@Override
	public BigDecimal getWithdrawalAmtPlan(String wismaCode, Date prsDate) {
		return prsDao.getWithdrawalAmtPlan(wismaCode, prsDate);
	}

	@Override
	public BigDecimal getBringMoney(String wismaCode, Date prsDate,String coCode) {
		return prsDao.getBringMoney(wismaCode, prsDate, coCode);
	}
	
	@Override
	public BigDecimal getplafon(Date disbrusmenDate, String wismaCode, String status) {
		// TODO Auto-generated method stub
		return loanDao.getplafon(disbrusmenDate, wismaCode, status);
	}
	
	@Override
	public BigDecimal getwithdrawalAmtNext(String wismaCode, Date prsDate) {
		// TODO Auto-generated method stub
		return prsDao.getwithdrawalamtplannext(wismaCode, prsDate);
	}

	@Override
	public List<PulangPRSpojo> getPulangPRS(String coCode, Date requestedDate) {
		List<PulangPRSpojo> results = null;
		try {
			Query query = this.em.createNativeQuery(pulangPRS, "PulangPRSpojo");
			query.setParameter("coCode", coCode);
			query.setParameter("requestedDate", requestedDate);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("pulang prs manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	@Override
	public List<PulangCollectionPojo> getPulangCollection(String coCode, Date requestedDate) {
		List<PulangCollectionPojo> results = null;
		try {
			Query query = this.em.createNativeQuery(pulangCollection, "PulangCollectionPojo");
			query.setParameter("coCode", coCode);
			query.setParameter("requestedDate", requestedDate);
			results = query.getResultList();
			em.close();
		} catch (Exception e) {
			log.error("pulang Collection manager impl ERROR : "+e.getMessage());
			em.close();
		}
		return results;
	}
	
	String pulangPRS = "select (sum(DISTINCT isnull(tr.amount,0))+isnull(z1.bring_money,0))-isnull(z.amount,0) as amount,s.sentra_name,p.sentra_id,m.code\n" + 
			"from t_report_trx_table tr\n" + 
			"inner join t_prs p on p.id=tr.prs_id\n" + 
			"inner join t_sentra s on s.id=p.sentra_id\n" + 
			"inner join m_location m on m.id=s.location_id\n" + 
			"LEFT join (\n" + 
			"select sum(DISTINCT isnull(amount,0)) as amount,s.sentra_name,s.id\n" + 
			"from t_report_trx_table tr\n" + 
			"inner join t_prs p on p.id=tr.prs_id\n" + 
			"inner join t_sentra s on s.id=p.sentra_id\n" + 
			"WHERE trx_type in (1,4) \n" + 
			"and p.prs_status in ('APPROVED','MANUAL_KFO','SUBMIT')\n" + 
			"and cast (business_dt as date) =:requestedDate \n" + 
			"and s.assign_to=:coCode\n" + 
			"and prs_id is NOT NULL \n" + 
			"GROUP by s.sentra_name,s.id\n" + 
			") z on s.id=z.id\n" + 
			"left join (\n" + 
			"select sum(isnull(bring_money,0)) as bring_money,p.sentra_id \n" + 
			"from t_prs p\n" + 
			"inner join t_sentra s on s.id=p.sentra_id\n" + 
			"WHERE p.prs_status in ('APPROVED','MANUAL_KFO','SUBMIT')\n" + 
			"and (cast (prs_date as date) =:requestedDate OR cast (update_date as date) =:requestedDate)\n" + 
			"and s.assign_to=:coCode\n" + 
			"group by p.sentra_id\n" + 
			") z1 on z1.sentra_id=s.id\n" + 
			"WHERE trx_type in (2,3,5) \n" + 
			"and cast (business_dt as date) =:requestedDate \n" + 
			"and prs_id is NOT NULL\n" + 
			"and p.prs_status in ('APPROVED','MANUAL_KFO','SUBMIT')\n" + 
			"and s.assign_to=:coCode\n" + 
			"GROUP by s.sentra_name,z.amount,p.sentra_id,z1.bring_money,m.code";

	String pulangCollection="select sum(x.amount) as amount,x.sentra_id,x.sentra_name,x.code from \n" + 
			"(\n" + 
			"select DISTINCT acc_number,sum(DISTINCT amount) amount,app_id,prs_id,p.sentra_id,s.sentra_name,m.code \n" + 
			"from t_report_trx_table tr\n" + 
			"inner join t_prs p on p.id=tr.prs_id\n" + 
			"inner join t_sentra s on s.id=p.sentra_id\n" + 
			"inner join m_location m on m.id=s.location_id\n" + 
			"WHERE trx_type in (2,4,5) \n" + 
			"and cast (business_dt as date) =:requestedDate and prs_id is NOT NULL \n" + 
			"and s.assign_to=:coCode\n" + 
			"GROUP by app_id,prs_id,acc_number,sentra_id,s.sentra_name,m.code\n" + 
			") x\n" + 
			"GROUP by x.sentra_id,x.sentra_name,x.code\n" + 
			"";

}